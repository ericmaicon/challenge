import React from 'react';
import ReactDOM from 'react-dom';
import Calendar from './Calendar/Calendar';

export default function layOutDay(events) {
  ReactDOM.render(<Calendar events={events} />, document.getElementById('root'));
}
