import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Calendar from './Calendar/Calendar';
import registerServiceWorker from './registerServiceWorker';
import layOutDay from './layOutDay';

const events = [
  {start: 30, end: 150},
  {start: 540, end: 600},
  {start: 560, end: 620},
  {start: 610, end: 670}
];

ReactDOM.render(<Calendar events={events} />, document.getElementById('root'));
registerServiceWorker();

window.layOutDay = layOutDay;
