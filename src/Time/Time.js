import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Time.css';

class Time extends Component {

  /**
   * render time container
   */
  render() {
    const { time } = this.props;

    return (
      <div className="Time">
        <div className="TimeHeader">
          <div className="TimeTitleContainer">
            <strong className="TimeTitle">{time.hour}</strong>
            <small className="TimeMeridiem">{time.meridiem}</small>
          </div>
          <div className="TimeHalfTime">{time.halftime}</div>
        </div>
        <div className="TimeContent">
        </div>
      </div>
    );
  }
}

Time.propTypes = {
  time: PropTypes.object.isRequired
};

export default Time;
