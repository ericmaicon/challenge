import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import Time from './Time';

const time = {
  hour: '09:00',
  meridiem: 'AM',
  halftime: '09:30',
}

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Time time={time} />, div);
});

it('renders the hour text', () => {
  const wrapper = shallow(<Time time={time} />);
  const container = wrapper.find('.TimeTitle').html();
  expect(container).toContain('09:00');
});
