import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import Event from './Event';

const event = {
  layout: {
    top: 0,
    left: 10,
    width: 200,
    height: 100
  }
}

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Event event={event} />, div);
});

it('should render on top', () => {
  const wrapper = shallow(<Event event={event} />);
  let containerStyle = wrapper.get(0).props.style;
  expect(containerStyle).toHaveProperty('top', 0);
});

it('should render on left', () => {
  const wrapper = shallow(<Event event={event} />);
  let containerStyle = wrapper.get(0).props.style;
  expect(containerStyle).toHaveProperty('left', 10);
});

it('should render with given width', () => {
  const wrapper = shallow(<Event event={event} />);
  let containerStyle = wrapper.get(0).props.style;
  expect(containerStyle).toHaveProperty('width', 200);
});

it('should render with given height', () => {
  const wrapper = shallow(<Event event={event} />);
  let containerStyle = wrapper.get(0).props.style;
  expect(containerStyle).toHaveProperty('height', 100);
});
