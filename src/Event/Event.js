import React, { Component } from "react";
import PropTypes from "prop-types";
import './Event.css';

class Event extends Component {
  /**
   * render the event with given layout props
   */
  render() {
    const { event } = this.props;

    return (
      <div
        className="Event"
        style={event.layout}
      >
        <strong className="EventTitle">Sample item</strong>
        <p className="EventSubTitle">Sample location</p>
      </div>
    );
  }
}

Event.propTypes = {
  event: PropTypes.object.isRequired
}

export default Event;
