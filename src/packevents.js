const _ = require("lodash/fp");

function overlapsAny(event, row) {
  if (row.length === 0) {
    return false;
  }

  return _.some(column => event.start < _.last(column).end, row);
}

function insertEvent(event, rows) {
  const lastRow = _.last(rows);
  const lastRowIndex = rows.length - 1;
  const bestColumnIndex = _.findLastIndex(column => event.start >= _.last(column).end, lastRow);

  return rows.map((row, rowIndex) => {
    if (bestColumnIndex === -1 && rowIndex === lastRowIndex) {
      return row.concat([[event]]);
    }

    if (rowIndex !== lastRowIndex) {
      return row;
    }

    return row.map((column, columnIndex) => {
      if (columnIndex === bestColumnIndex) {
        return column.concat([event]);
      }

      return column;
    });
  });
}

function packEvents(events) {
  return _.flow(
    _.sortBy("start"),
    _.reduce((rows, event) => {
      const lastRow = _.last(rows) || [];

      if (overlapsAny(event, lastRow)) {
        return insertEvent(event, rows);
      }

      return rows.concat([[[event]]]);
    }, [])
  )(events);
}


const events = [
  {start: 0, end: 60}, //9:00 - 10:00
  {start: 0, end: 60}, //9:00 - 10:00
  {start: 30, end: 600}, //9:30 - 19:00
  {start: 70, end: 90}, //10:10 - 10:30
  {start: 610, end: 670}, //19:10 - 20:10
  {start: 610, end: 600} //19:10 - 20:10
];

console.log(JSON.stringify(packEvents(events), null, 2));
