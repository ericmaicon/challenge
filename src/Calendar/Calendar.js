import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash/fp';
import './Calendar.css';
import Time from '../Time/Time';
import Event from '../Event/Event';

const MAX_TIME = 720;
const MAX_WIDTH = 600;
const PADDING_LEFT = 80;

class Calendar extends Component {
  /**
   * setup calendar times from 9AM to 9PM
   * @return {array} array of times
   */
  setupCalendarHours() {
    return _.flow(
      _.rangeRight(20),
      _.map((hour) => {
        let meridiem = 'AM';
        if (hour >= 12) {
          meridiem = 'PM';
          hour = (hour === 12 ? hour : hour-12);
        }

        return {
          hour: `${hour}:00`,
          meridiem: meridiem,
          halftime: `${hour}:30`
        }
      })
    )(8);
  }

  /**
   * filter events input
   *
   * @param  {object} event
   * @return {event|boolean}
   */
  filterEvents(event) {
    if (!event.hasOwnProperty("start") || !event.hasOwnProperty("end")) {
      return false;
    }

    if (_.isNaN(parseInt(event.start, 10)) || _.isNaN(parseInt(event.end, 10))) {
      return false;
    }

    if (event.end === 0) {
      return false;
    }

    if (event.start < 0) {
      return false;
    }

    if (event.start === event.end) {
      return false;
    }

    if (event.end < event.start) {
      return false;
    }

    if (event.end > MAX_TIME) {
      return false;
    }

    return event;
  }

  /**
   * check amount of colisions in a group of events
   *
   * @param  {object}  currentEvent event to check against the group
   * @param  {array}  groupedEvents group of events to check against the event
   * @return {integer} amount of collisions
   */
  hasCollision(currentEvent, groupedEvents) {
    let collisions = [];

    groupedEvents.forEach(event => {
      if (currentEvent.start < event.end && event.start < currentEvent.end) {
        collisions.push(event);
      }
    });

    return collisions;
  }

  /**
   * Filter events and sort by start time.
   *
   * @param  {array} events input
   * @return {array} array of grouped events.
   */
  setupEvents(events) {
    events = _.flow(
      _.filter(this.filterEvents),
      _.sortBy('start'),
    )(events);

    let column = 0;
    let eventsToTreat = events;
    let groupedEvents = [];
    do {
      const { rows, colidedEvents } = this.recursiveLoop(eventsToTreat, column);
      eventsToTreat = colidedEvents;
      column++;

      if (groupedEvents.length === 0) {
        groupedEvents = rows;
        continue;
      }

      groupedEvents.forEach(group => {
        rows.forEach((event) => {
          const firstEvent = _.first(event);
          if (this.hasCollision(firstEvent, [_.first(group)]).length > 0 && !firstEvent.colided) {
            firstEvent.colided = true;
            group.push(firstEvent);
          }
        });
      });
    } while (eventsToTreat.length);

    return groupedEvents;
  }

  /**
   * recursive loop to create array of rows. get the very first events and
   * group them without colide. If colide, return a new array with those.
   *
   * @param  {array} events array of items
   * @param  {integer} column column number
   * @return {object} an object with rows and colided events
   */
  recursiveLoop(events, column) {
    const rows = [];
    const colidedEvents = [];
    let currentGroup = [];

    //create the rows
    events.forEach(event => {
      event.column = column;

      if (currentGroup.length === 0) {
        currentGroup = [event];
      } else {
        if (this.hasCollision(event, currentGroup).length > 0) {
          colidedEvents.push(event);
        } else {
          rows.push(currentGroup);
          currentGroup = [event];
        }
      }
    });

    if (currentGroup.length >= 1) {
      rows.push(currentGroup);
    }

    return { rows, colidedEvents };
  }

  /**
   * check if has a colision in previous group. If it has and the amount is
   * bigger than the group size, get the same width.
   *
   * @param  {object} event the event
   * @param  {integer} groupSize current group size
   * @param  {array} previousGroup previous group of events
   * @return {integer} the width
   */
  calculateWidth(event, groupSize, previousGroup) {
    let width = (MAX_WIDTH - PADDING_LEFT) / groupSize;

    if (!previousGroup) {
      return width;
    }

    const collisions = this.hasCollision(event, previousGroup);
    if (collisions.length >= groupSize) {
      return _.first(collisions).layout.width;
    }

    return width;
  }

  /**
   * read groups, events and compute its layout
   *
   * @param  {array} groupedEvents group of events by row x column
   * @return {array} events with layout attributes
   */
  computeLayout(groupedEvents) {
    const events = [];

    groupedEvents.forEach(group => {
      group.forEach(event => {
        const width = this.calculateWidth(event, group.length, events);
        const left = PADDING_LEFT + (width * event.column);

        events.push({
          start: event.start,
          end: event.end,
          layout: {
            top: event.start,
            left,
            width,
            height: event.end - event.start
          }
        });
      });
    });

    return events;
  }

  /**
   * render the calendar
   */
  render() {
    const calendarHours = this.setupCalendarHours();
    let events = this.setupEvents(this.props.events);

    return (
      <div className="Calendar">
        {calendarHours.map((time, key) => (
          <Time key={key} time={time} />
        ))}
        {this.computeLayout(events).map((event, key) => (
          <Event
            key={key}
            event={event}
          />
        ))}
      </div>
    );
  }
}

Calendar.protTypes = {
  events: PropTypes.array.isRequired
}

export default Calendar;
