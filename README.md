# This is a front end programming challenge

The rules are:
- No events may visually overlap
- If two events collide in time, they MUST have the same width. This is an invariant. Call this
width W.
- W should be the maximum value possible without breaking the previous invariant.

## Table of Contents

- [Available Scripts](#available-scripts)
  - [npm start](#npm-start)
  - [npm test](#npm-test)
  - [npm run build](#npm-run-build)
  - [npm run eject](#npm-run-eject)
- [Why React?](#why-react)
- [Why not Moment.js?](#why-not-moment)

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

## Why React?

This is a test that needs render events in HTML. I could use no frameworks or libraries. What is the point of
NOT use a library that helps and give me power to render elements in an easy way?!

I could use jQuery or any other template parse framework. I choose React because:

1. Components are better than templates.
2. We can start small with React.
3. Fast render with Virtual DOM

## Why not Moment.js?

Moment.js could be used to render the time container and events as well. It is an useful framework, but I was
able to do everything without it, reducing the application size and library dependencies.

## External access

[You can check the project running, click here.](https://administrator-bruce-27131.netlify.com)
